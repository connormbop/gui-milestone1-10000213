﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_Console
{
    class Program
    {
        static Random Num = new Random();
        static double life = 5;
        static double score = 0;
        
        static void Main(string[] args)
        {        
            double UI;

            do
            {
                Console.WriteLine($"Score:{score} Life:{life}");
                Console.WriteLine("Pick a number between 1-5:");               
                UI = Double.Parse(Console.ReadLine());
                var x = Output(UI);
                Console.ReadLine();
                Console.Clear();
            } while (life > 0);
            Console.WriteLine($"You have run out of lives! your score was {score}");
            Console.ReadLine();
        }
        static double Output(double UI)
        {
            var a = 0;
            int NumRand = Num.Next(1, 5);
            if (NumRand == UI)
            {
                Console.WriteLine("Well Done!");
                score++;
                life--;
            }
            else
            {
                Console.WriteLine("Sorry maybe next time!");
                life--;
            }
            return a;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_Ui
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            int selector;
            double input;

            selector = int.Parse(textBox.Text);
            input = double.Parse(textBox1.Text);
            var x = choice(input, selector);
            textBlock2.Text = $"{convert}{x}";
        }
        //static string instruct1 = "1: Mile to KM Conversion";
        //static string instruct2 = "2: Km to Mile Conversion";
        static string convert = "Conversion:";
        static double choice(double input, int selector)
        {
            double a = 0;
            double total;
            switch (selector)
            {
                case 1: //km

                    total = input;
                    total = total * 1.609344;
                    a = total;
                    break;

                case 2: //mile
                    total = input;
                    total = total * 0.62137119;
                    a = total;
                    break;
            }
            return a;
        }
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_03_Uni
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            textBlock.Text = instruct1;
            textBlock1.Text = instruct2;
        }
        static string instruct1 = "Enter Age";
        static string instruct2 = "Enter Name";
        private void button_Click(object sender, RoutedEventArgs e)
        {
            var Name = "";
            int Age = 0;

            Name = textBox1.Text;

            Age = int.Parse(textBox.Text);
            var returnResult = Output(Age, Name);
            textBlock2.Text = returnResult;
        }
        static string Output(int Age, string Name)
        {
            string a = "";
            if (Age >= 18)
            {
                a = $"Hello {Name}, you may buy Alcohol";
            }
            else if (Age >= 16 || Age == 17)
            {
                a = $"Hello {Name}, You are Legal and can drink Alcohol but only with parental consent";
            }
            else if (Age < 16)
            {
                a = $"Hello {Name}, You are Too young to drink or buy alcohol";
            }
            else
            {
                a = "Something went Wrong Please enter your age in numbers";
            }

            return a;
        }
    }
}

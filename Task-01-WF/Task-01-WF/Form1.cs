﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_WF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            string Num;
            int selector = 0;
            
            var mileStr = "";
            double kmD;
            double mileD;
            
            InitializeComponent();
        }

        public void KMBTR_Click(object sender, EventArgs e)
        {
            var kmStr = "";
            double kmD;
            double total;
            kmStr = textBox1.Text;
            kmD = Double.Parse(kmStr);
            total = kmD * 1.609344;
            label1.Text = $"Kilometres:{total}";

        }

        public void MILEBTR_Click(object sender, EventArgs e)
        {
            var mileStr = "";
            double mileD;
            double total;
            mileStr = textBox1.Text;
            mileD = Double.Parse(mileStr);
            total = mileD * 0.62137119;
            label1.Text = $"Miles:{total}";
        }
    }
}

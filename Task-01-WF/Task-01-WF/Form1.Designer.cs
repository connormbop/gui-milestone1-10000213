﻿namespace Task_01_WF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.KMBTR = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.MILEBTR = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // KMBTR
            // 
            this.KMBTR.Location = new System.Drawing.Point(48, 173);
            this.KMBTR.Name = "KMBTR";
            this.KMBTR.Size = new System.Drawing.Size(75, 23);
            this.KMBTR.TabIndex = 0;
            this.KMBTR.Text = "To KM";
            this.KMBTR.UseVisualStyleBackColor = true;
            this.KMBTR.Click += new System.EventHandler(this.KMBTR_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(44, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // MILEBTR
            // 
            this.MILEBTR.Location = new System.Drawing.Point(172, 173);
            this.MILEBTR.Name = "MILEBTR";
            this.MILEBTR.Size = new System.Drawing.Size(75, 23);
            this.MILEBTR.TabIndex = 3;
            this.MILEBTR.Text = "To MILE";
            this.MILEBTR.UseVisualStyleBackColor = true;
            this.MILEBTR.Click += new System.EventHandler(this.MILEBTR_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.MILEBTR);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.KMBTR);
            this.Name = "Form1";
            this.Text = "KM-Mile Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button KMBTR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button MILEBTR;
    }
}


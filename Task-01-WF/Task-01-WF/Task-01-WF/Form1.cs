﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_WF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int selector;
            double input;

            selector = int.Parse(textBox1.Text);
            input = double.Parse(textBox2.Text);
            var x = choice(input, selector);
            label1.Text = $"{convert}{x}";
        }
        //static string instruct1 = "1: Mile to KM Conversion";
        //static string instruct2 = "2: Km to Mile Conversion";
        static string convert = "Conversion:";
        static double choice(double input, int selector)
        {
            double a = 0;
            double total;
            switch (selector)
            {
                case 1: //km

                    total = input;
                    total = total * 1.609344;
                    a = total;
                    break;

                case 2: //mile
                    total = input;
                    total = total * 0.62137119;
                    a = total;
                    break;
            }
            return a;
        }
    }
}

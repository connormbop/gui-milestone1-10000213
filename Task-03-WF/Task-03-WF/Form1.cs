﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_WF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label3.Text = instruct2;
            label1.Text = instruct1;
        }
        static string instruct1 = "Enter Age";
        static string instruct2 = "Enter Name";
        private void button1_Click(object sender, EventArgs e)
        {


            var Name = "";
            int Age = 0;

            Name = textBox2.Text;

            Age = int.Parse(textBox1.Text);
            var returnResult = Output(Age, Name);
            label2.Text = returnResult;



        }
        static string Output(int Age, string Name)
        {
            string a = "";
            if (Age >= 18)
            {
                a = $"Hello {Name}, you may buy Alcohol";
            }
            else if (Age >= 16 || Age == 17)
            {
                a = $"Hello {Name}, You are Legal and can drink Alcohol but only with parental consent";
            }
            else if (Age < 16)
            {
                a = $"Hello {Name}, You are Too young to drink or buy alcohol";
            }
            else
            {
                a = "Something went Wrong Please enter your age in numbers";
            }

            return a;
        }
    }
}
